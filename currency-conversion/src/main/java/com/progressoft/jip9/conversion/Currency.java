package com.progressoft.jip9.conversion;

import java.util.*;

public final class Currency {

    private static final Map<String, Currency> instanceMap;
    private static final Set<Currency> availableCurrencies;
    static {
        instanceMap = new HashMap<>();
        instanceMap.put("JOD", new Currency("JOD", "Jordanian Dinar"));
        instanceMap.put("USD", new Currency("USD", "US Dollar"));
        instanceMap.put("EUR", new Currency("EUR", "Euro"));
        availableCurrencies = Collections.unmodifiableSet(new HashSet<>(instanceMap.values()));
    }

    public static Currency getByCode(String code) {
        Objects.requireNonNull(code, "Null code was given");
        code = code.toUpperCase();
        if(!instanceMap.containsKey(code))
            throw new IllegalArgumentException("Unknown code '"+code+"' was given");
        return instanceMap.get(code);
    }

    public static Set<Currency> getAvailableCurrencies() {
        return availableCurrencies;
    }

    private final String code;
    private final String name;
    private final String toStringCache;

    public Currency(String code, String name) {
        this.code = code;
        this.name = name;
        toStringCache = code + ", " + name;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return toStringCache;
    }
}
