package com.progressoft.jip9.conversion;

import java.math.BigDecimal;

public interface RateValueSupplier {
    BigDecimal getRateOf(Currency base, Currency quote);
}
