package com.progressoft.jip9.conversion;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class ExchangeRatePool {

    private final RateValueSupplier supplier;
    private final Map<CurrencyPair, ExchangeRate> cachedRates;
    public ExchangeRatePool(RateValueSupplier supplier) {
        this.supplier = Objects.requireNonNull(supplier, "A null RateValueSupplier was given");
        cachedRates = new HashMap<>();
    }

    public ExchangeRate getOf(Currency base, Currency quote) {
        CurrencyPair pair = new CurrencyPair(base, quote);
        if (!cachedRates.containsKey(pair)) {
            BigDecimal rateValue = supplier.getRateOf(base, quote);
            ExchangeRate result = ExchangeRate.createAndCreateReverse(pair, rateValue);
            cachedRates.put(pair, result);
            cachedRates.put(pair.reverse(), result.reverse());
            return result;
        }
        return cachedRates.get(pair);
    }

    public BigDecimal apply(Currency base, Currency quote, BigDecimal moneyToExchange) {
        return getOf(base,quote).apply(moneyToExchange);
    }
}
