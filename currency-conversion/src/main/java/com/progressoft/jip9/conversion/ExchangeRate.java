package com.progressoft.jip9.conversion;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Objects;

public class ExchangeRate {
    public static final int SCALE = 4;

    public static ExchangeRate createAndCreateReverse(CurrencyPair pair, BigDecimal rateValue) {
        validatePairAndRate(pair, rateValue);
        rateValue = rateValue.setScale(SCALE, RoundingMode.HALF_EVEN);
        ExchangeRate result = new ExchangeRate(pair, rateValue);

        createReverse(result);
        return result;
    }

    private static void createReverse(ExchangeRate exchangeRate) {
        BigDecimal reverseRateValue = BigDecimal.ONE.divide(exchangeRate.value, SCALE, RoundingMode.HALF_EVEN);
        CurrencyPair reversePair = exchangeRate.pair.reverse();
        ExchangeRate reverse = new ExchangeRate(reversePair, reverseRateValue);
        exchangeRate.reverse = reverse;
        reverse.reverse = exchangeRate;
    }

    private static void validatePairAndRate(CurrencyPair pair, BigDecimal rateValue) {
        Objects.requireNonNull(pair, "CurrencyPair was null");
        Objects.requireNonNull(rateValue, "rate was null");
        if (rateValue.signum() < 0) {
            throw new IllegalArgumentException("rate was negative");
        }
    }

    private final CurrencyPair pair;
    // TODO create Money class to fix scale issues
    private final BigDecimal value;
    private ExchangeRate reverse;

    private ExchangeRate(CurrencyPair pair, BigDecimal value) {
        this.pair = pair;
        this.value = value;
    }

    public ExchangeRate reverse() {
        return reverse;
    }

    public BigDecimal getValue() {
        return value;
    }

    public Currency getBase() {
        return pair.getBase();
    }

    public Currency getQuote() {
        return pair.getQuote();
    }

    public BigDecimal apply(BigDecimal moneyToExchange) {
        return moneyToExchange.multiply(value).setScale(SCALE, RoundingMode.HALF_EVEN);
    }
}
