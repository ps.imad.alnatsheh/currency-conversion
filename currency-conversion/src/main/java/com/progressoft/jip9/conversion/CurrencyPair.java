package com.progressoft.jip9.conversion;

import java.util.Objects;

public class CurrencyPair {
    private final Currency base, quote;

    public CurrencyPair(Currency base, Currency quote) {
        this.base = Objects.requireNonNull(base, "base was null");
        this.quote = Objects.requireNonNull(quote, "quote was null");
    }

    public Currency getBase() {
        return base;
    }

    public Currency getQuote() {
        return quote;
    }

    @Override
    public boolean equals(Object other) {
        if (other == this)
            return true;
        if (other == null)
            return false;
        if(getClass() != other.getClass())
            return false;
        CurrencyPair otherPair = (CurrencyPair) other;
        return base.equals(otherPair.base) &&
                quote.equals(otherPair.quote);
    }

    @Override
    public int hashCode() {
        return Objects.hash(base, quote);
    }

    public CurrencyPair reverse() {
        return new CurrencyPair(quote, base);
    }
}
