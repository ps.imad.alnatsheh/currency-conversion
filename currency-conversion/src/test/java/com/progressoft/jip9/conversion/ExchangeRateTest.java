package com.progressoft.jip9.conversion;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class ExchangeRateTest {
    @Test
    public void givenNullCurrencyPairAndValidRate_whenCreateExchangeRate_thenThrowException() {
        CurrencyPair pair = null;
        BigDecimal rate = BigDecimal.valueOf(1);
        NullPointerException ex = Assertions.assertThrows(NullPointerException.class,
                () -> ExchangeRate.createAndCreateReverse(pair, rate));
        Assertions.assertEquals("CurrencyPair was null", ex.getMessage());
    }

    @Test
    public void givenCurrencyPairAndInvalidRate_whenCreateExchangeRate_thenThrowException() {
        String jodCode = "JOD";
        String usdCode = "USD";
        Currency jod = Currency.getByCode(jodCode);
        Currency usd = Currency.getByCode(usdCode);
        CurrencyPair pair = new CurrencyPair(jod, usd);

        BigDecimal nullRate = null;
        Exception ex = Assertions.assertThrows(NullPointerException.class,
                () -> ExchangeRate.createAndCreateReverse(pair, nullRate));
        Assertions.assertEquals("rate was null", ex.getMessage());

        BigDecimal negativeRate = BigDecimal.valueOf(-1);
        ex = Assertions.assertThrows(IllegalArgumentException.class,
                () -> ExchangeRate.createAndCreateReverse(pair, negativeRate));
        Assertions.assertEquals("rate was negative", ex.getMessage());
    }

    @Test
    public void givenValidInputs_whenCreateExchangeRate_thenReturnExpectedResult() {
        String jodCode = "JOD";
        String usdCode = "USD";
        Currency jod = Currency.getByCode(jodCode);
        Currency usd = Currency.getByCode(usdCode);
        CurrencyPair pair = new CurrencyPair(jod, usd);
        BigDecimal rateValue = BigDecimal.valueOf(5);

        ExchangeRate exchangeRate = ExchangeRate.createAndCreateReverse(pair, rateValue);
        Assertions.assertNotNull(exchangeRate, "returned ExchangeRate was null");
        Assertions.assertEquals(jod, exchangeRate.getBase(), "returned base was not as expected");
        Assertions.assertEquals(usd, exchangeRate.getQuote(), "returned quote was not as expected");
        Assertions.assertEquals(usd, exchangeRate.getQuote(), "returned quote was not as expected");
        Assertions.assertEquals(0, rateValue.compareTo(exchangeRate.getValue()), "value of exchangeRate was not as expected");
        Assertions.assertEquals(4, exchangeRate.getValue().scale(), "The scale of the value of ExchangeRate was not 4");

        ExchangeRate reversedRate = exchangeRate.reverse();
        Assertions.assertNotNull(reversedRate, "returned ExchangeRate was null");
        Assertions.assertEquals(usd, reversedRate.getBase(), "returned base was not as expected");
        Assertions.assertEquals(jod, reversedRate.getQuote(), "returned quote was not as expected");
        BigDecimal expectedReverseValue = BigDecimal.ONE.divide(exchangeRate.getValue(), 4, RoundingMode.HALF_EVEN);
        Assertions.assertEquals(expectedReverseValue, reversedRate.getValue(), "value of reversedRate wasn't as expected");
        Assertions.assertEquals(4, reversedRate.getValue().scale(), "The scale of the value of ExchangeRate was not 4");
    }

    @Test
    public void givenExchangeRateAndMoneyToExchange_whenApply_thenReturnExpectedResult() {
        String jodCode = "JOD";
        String usdCode = "USD";
        Currency jod = Currency.getByCode(jodCode);
        Currency usd = Currency.getByCode(usdCode);
        CurrencyPair pair = new CurrencyPair(usd, jod);
        BigDecimal rateValue = new BigDecimal("0.7100");
        ExchangeRate exchangeRate = ExchangeRate.createAndCreateReverse(pair, rateValue);

        BigDecimal moneyToExchange = new BigDecimal("13.4000");
        BigDecimal result = exchangeRate.apply(moneyToExchange);

        BigDecimal expected = new BigDecimal("9.5140");
        Assertions.assertEquals(expected, result, "result of apply was not as expected");
    }
}
