package com.progressoft.jip9.conversion;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Set;

public class CurrencyTest {

    @Test
    public void givenInvalidCode_whenGetCurrency_thenThrowException() {
        String nullCode = null;
        Exception ex = Assertions.assertThrows(NullPointerException.class,
                () -> Currency.getByCode(nullCode));
        Assertions.assertEquals("Null code was given", ex.getMessage());

        String unknownCode = "GIBBERISH";
        ex = Assertions.assertThrows(IllegalArgumentException.class,
                () -> Currency.getByCode(unknownCode));
        Assertions.assertEquals("Unknown code 'GIBBERISH' was given", ex.getMessage());
    }

    @Test
    public void givenValidCode_whenGetCurrency_thenReturnExpectedResult() {
        String jodCode = "jod";
        Currency jod = Currency.getByCode(jodCode);
        Assertions.assertNotNull(jod, "Given Currency was null");
        Assertions.assertEquals("JOD", jod.getCode(), "Given Currency had the wrong code");
        Assertions.assertEquals("Jordanian Dinar", jod.getName(), "Given Currency had the wrong name");

        String jodCode2 = "jOd";
        String usdCode = "USD";
        Currency jod2 = Currency.getByCode(jodCode2);
        Currency usd = Currency.getByCode(usdCode);
        Assertions.assertTrue(jod == jod2, "Two Currency instances with the same code exist");
        Assertions.assertFalse(jod == usd, "Different codes gave the same Currency");
    }

    @Test
    public void givenCurrency_whenToString_thenReturnExpectedResult() {
        Currency jod = Currency.getByCode("JOD");
        String result = jod.toString();
        Assertions.assertEquals("JOD, Jordanian Dinar", result, "toString Result was not as expected");
    }

    @Test
    public void whenModifyAvailableCurrencies_thenThrowException() {
        Currency jod = Currency.getByCode("JOD");
        Set<Currency> currencies = Currency.getAvailableCurrencies();
        Assertions.assertThrows(UnsupportedOperationException.class,
                () -> currencies.add(jod), "available currencies is modifiable");
    }

    @Test
    public void whenGetAvailableCurrencies_thenReturnExpectedResult() {
        Currency jod = Currency.getByCode("JOD");
        Currency usd = Currency.getByCode("USD");
        Set<Currency> currencies = Currency.getAvailableCurrencies();
        Assertions.assertTrue(currencies.contains(jod), "Currency was not contained in available currencies set");
        Assertions.assertTrue(currencies.contains(usd), "Currency was not contained in available currencies set");
    }
}