package com.progressoft.jip9.conversion;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class CurrencyPairTest {
    @Test
    public void givenNullBaseCurrencyAndValidQuote_whenCreateCurrencyPair_thenThrowException() {
        String jodCode = "JOD";
        Currency jod = Currency.getByCode(jodCode);
        Currency nullCurrency = null;
        Exception ex = Assertions.assertThrows(NullPointerException.class,
                () -> new CurrencyPair(nullCurrency, jod));
        Assertions.assertEquals("base was null", ex.getMessage());
    }

    @Test
    public void givenValidBaseCurrencyAndNullQuote_whenCreateCurrencyPair_thenThrowException() {
        String jodCode = "JOD";
        Currency jod = Currency.getByCode(jodCode);
        Currency nullCurrency = null;
        Exception ex = Assertions.assertThrows(NullPointerException.class,
                () -> new CurrencyPair(jod, nullCurrency));
        Assertions.assertEquals("quote was null", ex.getMessage());
    }

    @Test
    public void givenValidCurrencies_whenCreateCurrencyPair_thenReturnExpectedResult() {
        String jodCode = "JOD";
        String usdCode = "USD";
        Currency jod = Currency.getByCode(jodCode);
        Currency usd = Currency.getByCode(usdCode);
        CurrencyPair pair = new CurrencyPair(jod, usd);

        Assertions.assertEquals(jod, pair.getBase(), "returned base currency was not as expected");
        Assertions.assertEquals(usd, pair.getQuote(), "returned quote currency was not as expected");
    }


    @Test
    public void givenCurrencyPair_whenReverse_thenReturnExpectedResult() {
        String jodCode = "JOD";
        String usdCode = "USD";
        Currency jod = Currency.getByCode(jodCode);
        Currency usd = Currency.getByCode(usdCode);
        CurrencyPair pair = new CurrencyPair(jod, usd);
        CurrencyPair reverse = pair.reverse();
        CurrencyPair expected = new CurrencyPair(usd, jod);

        Assertions.assertEquals(expected, reverse, "reverse CurrencyPair was not as expected");
    }

    @Test
    public void givenTwoCurrencyPairs_whenEqualOrHashcode_thenReturnExpectedResult() {
        String jodCode = "JOD";
        String usdCode = "USD";
        Currency jod = Currency.getByCode(jodCode);
        Currency usd = Currency.getByCode(usdCode);
        CurrencyPair pair1 = new CurrencyPair(jod, usd);
        CurrencyPair pair2 = new CurrencyPair(jod, usd);
        CurrencyPair pair3 = new CurrencyPair(jod, jod);

        Assertions.assertFalse(pair1.equals(null), "CurrencyPair was equal to null");
        Assertions.assertTrue(pair1.equals(pair1), "CurrencyPair was not equal to itself");
        Assertions.assertTrue(pair1.equals(pair2), "CurrencyPairs were not equal when they should be");
        Assertions.assertEquals(pair1.hashCode(), pair2.hashCode(), "CurrencyPairs were equal but their hashcode was not");
        Assertions.assertFalse(pair1.equals(pair3), "CurrencyPairs were equal when they should not be");
        Assertions.assertFalse(pair1.equals("something"), "CurrencyPair was equal to a string");
    }
}
