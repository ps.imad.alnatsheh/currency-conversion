package com.progressoft.jip9.conversion;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class ExchangeRatePoolTest {

    private static RateValueSupplier testRateValueSupplier;

    @BeforeAll
    public static void setup() {
        testRateValueSupplier = ExchangeRatePoolTest::getRandomRate;
    }

    public static BigDecimal getRandomRate(Currency a, Currency b) {
        return BigDecimal.valueOf(Math.random() * 10);
    }

    @Test
    public void givenNullRateValueSupplier_whenCreateExchangeRatePool_thenThrowException() {
        RateValueSupplier nullSupplier = null;
        NullPointerException ex = Assertions.assertThrows(NullPointerException.class,
                () -> new ExchangeRatePool(nullSupplier));
        Assertions.assertEquals("A null RateValueSupplier was given", ex.getMessage());
    }

    @Test
    public void givenTwoCurrencies_whenGetExchangeRate_thenReturnExpectedResult() {
        Currency jod = Currency.getByCode("JOD");
        Currency usd = Currency.getByCode("USD");

        ExchangeRatePool pool = new ExchangeRatePool(testRateValueSupplier);

        ExchangeRate rate = pool.getOf(usd, jod);
        ExchangeRate rateReversed = pool.getOf(jod, usd);
        Assertions.assertNotNull(rate, "returned ExchangeRate was null");
        Assertions.assertEquals(usd, rate.getBase(), "returned base currency was not as expected");
        Assertions.assertNotNull(rateReversed, "returned ExchangeRate was null");
        Assertions.assertSame(rateReversed, rate.reverse(), "two different ExchangeRate instances with the same currencies exist");
    }

    @Test
    public void givenTwoCurrenciesAndMoneyToExchange_whenApply_thenReturnExpectedResult() {
        String jodCode = "JOD";
        String usdCode = "USD";
        Currency jod = Currency.getByCode(jodCode);
        Currency usd = Currency.getByCode(usdCode);
        RateValueSupplier supplier = (b, q) -> new BigDecimal("0.7100");
        ExchangeRatePool pool = new ExchangeRatePool(testRateValueSupplier);

        ExchangeRate rate = pool.getOf(usd, jod);

        BigDecimal moneyToExchange = new BigDecimal("13.4000");
        BigDecimal result = pool.apply(usd, jod, moneyToExchange);

        BigDecimal expected = rate.apply(moneyToExchange);
        Assertions.assertEquals(expected, result, "result of apply was not as expected");
    }

    // TODO add ExchangeRatePool.update to re-request certain values
}
